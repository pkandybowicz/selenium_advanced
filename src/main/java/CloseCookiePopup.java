import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CloseCookiePopup {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://allegro.pl");
        driver.manage().window().maximize();
        Thread.sleep(1000);
        closeCookiePopup(driver);
        Thread.sleep(1000);

        driver.quit();
    }

    private static void closeCookiePopup(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        List<WebElement> cookieButtons = driver.findElements(By.xpath("//*[@data-role=\"accept-consent\"]"));
        if (cookieButtons.size() > 0) {
            cookieButtons.get(0).click();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
}
