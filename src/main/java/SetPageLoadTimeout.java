import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class SetPageLoadTimeout {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        driver.get("https://allegro.pl");
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.get("https://www.google.pl/");
        driver.quit();
    }
}
