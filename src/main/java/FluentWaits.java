import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.function.Function;

public class FluentWaits {

    public static void main(String[] args) throws InterruptedException {

        WebDriver driver = new ChromeDriver();
        driver.get("https://allegro.pl/"); //manual change to tvn24.pl to found element

        Wait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(10))
                .ignoring(NoSuchElementException.class);

        WebElement rodoApprove = fluentWait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                System.out.println("checking...");
                WebElement element = driver.findElement(By.id("onetrust-accept-btn-handler"));
                System.out.println("found element");
                return element;
            }
        });

        rodoApprove.click();
        Thread.sleep(2000);
        driver.quit();

    }
}
