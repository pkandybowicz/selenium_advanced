import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DragAndDrop {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://jqueryui.com/draggable/");
        driver.switchTo().frame(0);

        WebElement draggableElement = driver.findElement(By.id("draggable"));

        Actions builder = new Actions(driver);

        builder.dragAndDropBy(draggableElement, 200, 200).build().perform();
        builder.dragAndDropBy(draggableElement, -100, -150).build().perform();

        builder.clickAndHold(draggableElement).moveByOffset(50, 50).release().build().perform();


        Thread.sleep(1000);
        driver.quit();
    }
}
