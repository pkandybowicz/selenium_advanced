import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MultipleTabs {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://www.w3schools.com/tags/att_a_target.asp");
        driver.findElement(By.id("accept-choices")).click();

        driver.findElement(By.linkText("Try it Yourself »")).click();

        Set<String> windowHandles = driver.getWindowHandles();
        driver.switchTo().window(windowHandles.toArray()[1].toString());

        driver.findElement(By.xpath("//*[text()='Run »']")).click();

        Thread.sleep(1000);
        driver.close();
        Thread.sleep(2000);
        driver.quit();
    }
}
