import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ToolTipsChecks {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.w3schools.com/css/css_tooltip.asp");

        driver.findElement(By.id("accept-choices")).click();

        List<WebElement> elements = driver.findElements(By.className("tooltip"));
        List<WebElement> tooltips = driver.findElements(By.xpath("//*[@class='tooltip']/span"));

        Actions builder = new Actions(driver);

        Assertions.assertFalse(tooltips.get(0).isDisplayed());
        builder.moveToElement(elements.get(0)).build().perform();
        Assertions.assertTrue(tooltips.get(0).isDisplayed());

        Assertions.assertFalse(tooltips.get(1).isDisplayed());
        builder.moveToElement(elements.get(1)).build().perform();

        Assertions.assertFalse(tooltips.get(0).isDisplayed());
        Assertions.assertTrue(tooltips.get(1).isDisplayed());

        Thread.sleep(1000);
        driver.quit();
    }
}
