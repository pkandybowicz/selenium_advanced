import com.google.common.io.Files;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

public class ExecuteJavaScript {
    public static void main(String[] args) throws InterruptedException {

        WebDriver driver = new ChromeDriver();

        driver.get("https://allegro.pl");

        JavascriptExecutor js = (JavascriptExecutor) driver;

        Thread.sleep(1000);
        js.executeScript("window.scrollBy(0,1000)");
        Thread.sleep(1000);
        js.executeScript("window.scrollBy(0,-1000)");
        Thread.sleep(1000);

        driver.quit();
    }
}
