import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class IFrame {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://phptravels.com/");


        WebElement chatIFrame2 = driver.findElement(By.xpath("(//iframe[@title='chat widget'])[2]"));
        chatIFrame2.click();

        WebElement chatIFrame1 = driver.findElement(By.xpath("(//iframe[@title='chat widget'])[1]"));
        driver.switchTo().frame(chatIFrame1);
    
        driver.findElement(By.id("offline0Field")).sendKeys("Adam");
        Thread.sleep(2000);
        driver.quit();

    }
}
